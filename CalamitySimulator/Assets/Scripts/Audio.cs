﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
    public AudioClip endScreenMusic;

    public AudioSource MusicSource;

    // Start is called before the first frame update
    void Start()
    {
        MusicSource.clip = endScreenMusic;
        MusicSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
    }
}