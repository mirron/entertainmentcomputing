﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Autor Oliver Sander
/// <summary>
/// checks if the player was in a safe zone during the eathquake
/// </summary>
public class EarthquakeSafeZone : MonoBehaviour
{

    private static bool enteredeDuringEarthquake = false;
    private static bool leftDuringEarthquake = false;
    private static bool earthQuakeWasLogged = false;
    // private bool leftAfterEarthquake = true;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (GlobalFlagScript.isEarthquake)
        {
            enteredeDuringEarthquake = true;
        }
        else
        {
            enteredeDuringEarthquake = false;
            //leftAfterEarthquake = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (GlobalFlagScript.isEarthquake)
        {
            if (!leftDuringEarthquake && !enteredeDuringEarthquake)
            {
                enteredeDuringEarthquake = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (GlobalFlagScript.isEarthquake && !earthQuakeWasLogged)
        {
            leftDuringEarthquake = true;
            Logging.setDoneAction(new Action("Door You should not leave the safe area during an earthquake", 0, null, possibleActions.LeftSafeZoneDuringEarthquake));
            earthQuakeWasLogged = true;
        }
        if (!leftDuringEarthquake && !GlobalFlagScript.isEarthquake && enteredeDuringEarthquake && !earthQuakeWasLogged)
        {
            Logging.setDoneAction(new Action("Door Stayed in safe zone during earthquake", 0, null, possibleActions.HideUnderDoorway));
            earthQuakeWasLogged = true;
        }
    }
}
