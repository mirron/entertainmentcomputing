﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorFireScript : MonoBehaviour
{
    private float waitTime = 2.0f;
    private float timer = 0.0f;
    public AudioClip fireClip;
    public AudioSource fireSource;
    // Start is called before the first frame update
    void Start()
    {
        fireSource.clip = fireClip;
        fireSource.Play();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        //Debug.Log("Is in Stay");
        if (other.gameObject.tag == "FireExtinguisher")
        {
            // Debug.Log("Is in Fire");
            timer += Time.deltaTime;

            if (timer > waitTime)
            {
                // Debug.Log("Is in timer");
                Destroy(this.gameObject);
                // I am not sure if I can add the fire as gameobject, because it gets destroyed before. Maybe we need to write a cloneing method
                // Also I am not sure how exacly Destroy works for this scenario
                Logging.setDoneAction(new Action("Door Fire was extingusihed", 0, null, possibleActions.DoorFireExtinguished));
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Is in Stay");
        if (other.gameObject.tag == "Player")
        {
            // TODO: Make the player Lose
            SceneManager.LoadScene(GlobalFlagScript.EndScene);
            MainGameLogic.fireManager.ResetSpawnedFire();
            Logging.setDoneAction(new Action("You shall not walk trough the fire and the flame", 0, this.gameObject, possibleActions.WalkedIntoFire));
        }
    }
}
