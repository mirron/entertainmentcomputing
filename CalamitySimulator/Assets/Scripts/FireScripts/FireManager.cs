﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Author Oliver Sander

/// <summary>
/// Still a test wich needs to be extended
/// </summary>
public class FireManager
{
    private string resourcePathDoor = "Prefabs/DoorFire";
    private string resourcePathNormal = "Prefabs/NormalFire";
    private List<Vector3> spawnedFire = new List<Vector3>();

    public FireManager()
    {
        //GameObject fireGameObject = UnityEngine.Object.Instantiate(Resources.Load(resourcePath),
        //new Vector3(2, 0, 2), Quaternion.identity) as GameObject;

        //ParticleSystem ps = fireGameObject.GetComponent<ParticleSystem>();
        //ps.Play();

    }

    public void SpawnFire()
    {
        if (!spawnedFire.Contains(GlobalFlagScript.doorFirePosition))
        {
            GameObject fireGameObject = UnityEngine.Object.Instantiate(Resources.Load(resourcePathDoor),
            GlobalFlagScript.doorFirePosition, Quaternion.identity) as GameObject;
            spawnedFire.Add(GlobalFlagScript.doorFirePosition);
        }


        foreach (Vector3 position in GlobalFlagScript.firePositions)
        {
            if (!spawnedFire.Contains(position))
            {
                GameObject newFireGameObject = UnityEngine.Object.Instantiate(Resources.Load(resourcePathNormal),
                position, Quaternion.identity) as GameObject;
                spawnedFire.Add(position);
            }

        }
    }

    public void ResetSpawnedFire()
    {
        spawnedFire = new List<Vector3>();
    }
}
