﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NonExtinguishable : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Is in Stay");
        if (other.gameObject.tag == "Player")
        {
            // TODO: Make the player Lose
            Logging.setDoneAction(new Action("You shall not walk trough the fire and the flame", 0, this.gameObject, possibleActions.WalkedIntoFire));
            SceneManager.LoadScene(GlobalFlagScript.EndScene);
        }
    }
}
