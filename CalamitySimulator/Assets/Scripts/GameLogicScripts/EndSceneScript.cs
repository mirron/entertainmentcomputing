﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Cursor = UnityEngine.Cursor;

// Author Oliver Sander
namespace GameLogicScripts
{
    /// <summary>
    /// TODO: Right now only debug messages are shown for testing puposes. Also I am not shure how we want to make the endscren look.
    /// </summary>
    public class EndSceneScript : MonoBehaviour
    {
        private Canvas canvas;

        private string win = "You survived!";
        private string loose = "You died!";

        // Start is called before the first frame update
        void Start()
        {
            Cursor.lockState = CursorLockMode.None; ;
            Canvas canvas = getChangeableCanvas();
            ActionEvaluation actionEvaluation = new ActionEvaluation();
            setWintext();
            setActions(getScrollViewText(canvas),actionEvaluation.EvaluteAllActions());
            setScoreText(actionEvaluation.getPoints());
        }

        private void setScoreText(String points)
        {
            Text scoreText = canvas.transform.Find("TotalScoreText").GetComponent<Text>();
            scoreText.GetComponent<Text>().text = points;
        }

        private void setWintext()
        {
            Text wintext = canvas.transform.Find("text_Win_or_Loose").GetComponent<Text>();
            if (Logging.win)
            {
                wintext.GetComponent<Text>().text = win;
            }
            else
            {
                wintext.GetComponent<Text>().text = loose;
            }
        }

        private Canvas getChangeableCanvas()
        {
            GameObject canvasGo = GameObject.Find("Canvas");
            canvas = canvasGo.GetComponent<Canvas>();
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            return canvas;
        }


        private Text getScrollViewText(Canvas canvas)
        {
            return canvas.transform.Find("ActionsScroller").Find("PortView").Find("Content_Action_Text").GetComponent<Text>();
        }

        private void setActions(Text actionsText, String actions)
        {
            actionsText.text = actions;
        }
    
    }
}