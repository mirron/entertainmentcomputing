﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainGameLogic : MonoBehaviour
{

    private static float timer = 0.0f;
    public float timeBeforeEarthquake = 2.0f;
    public float erathQuakeTimer = 5.0f;
    public float maxPlayTime = 120.0f;
    public static FireManager fireManager;

    [SerializeField] public AudioClip fireAlarmClip;
    [SerializeField] public AudioSource fireAlarmSource;
    [SerializeField] public AudioClip earthquakeClip;
    [SerializeField] public AudioSource earthquakeSource;
    [SerializeField] public AudioClip sirenClip;
    [SerializeField] public AudioSource sirenSource;

    private Vector3 spreadFirePostion1 = new Vector3(-3.0f, 0.0f, -12.0f);
    private Vector3 spreadFirePostion2 = new Vector3(-3.0f, 0.0f, -10.0f);
    private Vector3 spreadFirePostion3 = new Vector3(-5.0f, 0.0f, -12.5f);
    private Vector3 spreadFirePostion4 = new Vector3(-3.0f, 0.0f, -8.0f);

    // Start is called before the first frame update
    void Start()
    {
        timer = 0;
        PlayerInteractWithObjects.isGrapping = false;
        GlobalFlagScript.ResetAllFlags();
        Logging.ResetLogger();
        Logging.setDoneAction(new Action("Game started", timer, null, possibleActions.GameStart));
        GlobalFlagScript.doorFirePosition = new Vector3(-1.695f, 0f, -11.80f);
        //GlobalFlagScript.firePositions.Add(new Vector3(-2.3f, 0.1f, -14f));

        fireManager = new FireManager();
        fireAlarmSource.clip = fireAlarmClip;
        earthquakeSource.clip = earthquakeClip;
        sirenSource.clip = sirenClip;
        sirenSource.Play();
       
       //PlayerInteractWithObjects.isEarthquake = true;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (GlobalFlagScript.enableCheating == false) { 
            if (timer > timeBeforeEarthquake && timer < (timeBeforeEarthquake + erathQuakeTimer) && !GlobalFlagScript.isEarthquake)
            {
                StartEarthquake();
            }
            else if (GlobalFlagScript.isEarthquake && timer > (timeBeforeEarthquake + erathQuakeTimer))
            {
                StopEarthquake();
                fireAlarmSource.Play();
                fireManager.SpawnFire();
            }
        }

        if (GlobalFlagScript.isEarthquake && !earthquakeSource.isPlaying)
        {
            earthquakeSource.Play();
        }
        if (!GlobalFlagScript.isEarthquake && earthquakeSource.isPlaying)
        {
            earthquakeSource.Stop();
        }

        if (timer > maxPlayTime)
        {
            Logging.setDoneAction(new Action("You took to long and could not breath becuase of the smoke", 0, this.gameObject, possibleActions.SmokedOut));
            SceneManager.LoadScene(GlobalFlagScript.EndScene);
           
        }
        
        if (timer > 25.0f && !GlobalFlagScript.firePositions.Contains(spreadFirePostion1)) 
        {
            GlobalFlagScript.firePositions.Add(spreadFirePostion1);
            fireManager.SpawnFire();
        }
        if (timer > 50.0f && !GlobalFlagScript.firePositions.Contains(spreadFirePostion2))
        {
            GlobalFlagScript.firePositions.Add(spreadFirePostion2);
            fireManager.SpawnFire();
        }
        if (timer > 75.0f && !GlobalFlagScript.firePositions.Contains(spreadFirePostion3))
        {
            GlobalFlagScript.firePositions.Add(spreadFirePostion3);
            fireManager.SpawnFire();
        }
        if (timer > 100.0f && !GlobalFlagScript.firePositions.Contains(spreadFirePostion4)) 
        {
            GlobalFlagScript.firePositions.Add(spreadFirePostion4);
            fireManager.SpawnFire();
        }
    }

    /// <summary>
    /// On playerdead call this method, call endscreen with list of logged actions and format nice
    /// </summary>
    void EndGame()
    {
        Logging.setDoneAction(new Action("Game ended", timer, null, possibleActions.GameEnd));
    }

    public static float GetTimer()
    {
        return timer;
    }

    public static void StartEarthquake()
    {
        GlobalFlagScript.isEarthquake = true;
    }

    public static void StopEarthquake()
    {
        GlobalFlagScript.isEarthquake = false;
    }
}
