﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace GameLogicScripts
{
    public class Menu : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }

        public void PlayGame()
        {
            SceneManager.LoadScene("Level1");
        }

        public void GoToMainMenu()
        {
            SceneManager.LoadScene("MainMenuScreen");
        }

        public void QuitGame()
        {
            Application.Quit();
        }
    }
}
