﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalFlagScript : MonoBehaviour
{

    public static Vector3 doorFirePosition;
    public static List<Vector3> firePositions = new List<Vector3>();
    public static bool isMovementFrozen = false;
    public static string EndScene = "LevelFinished";
    public static bool isEarthquake = false;
    public static bool enableCheating = false;
    public static bool isEarthquakeManipulated = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void ResetAllFlags()
    {
        isMovementFrozen = false;
        isEarthquake = false;
        enableCheating = false;
        isEarthquakeManipulated = false;
    }

}
