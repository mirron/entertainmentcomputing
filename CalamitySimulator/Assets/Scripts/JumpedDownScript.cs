﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class JumpedDownScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Is in Stay");
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "testPlayerImmune")
        {
            Logging.win = true;
            Logging.setDoneAction(new Action("Jumping Down can be a valid choise, but you a risking to break your legs, or worse", 0, null, possibleActions.WinByJumping));
            SceneManager.LoadScene(GlobalFlagScript.EndScene);
        }
    }
}
