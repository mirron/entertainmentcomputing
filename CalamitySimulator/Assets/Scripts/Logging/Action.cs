﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

//Author Tim Kratky
/// <summary>
/// Class used to describe playeractions that matter for the rating of the playthrough and the endscreen
/// </summary>
public class Action 
{
    private String actionDescribtion;
    private float timeOfAction;
    private UnityEngine.GameObject gameObjectOfAction;
    private possibleActions enumForAction;

    /// <summary>
    /// Constructor of action
    /// </summary>
    /// <param name="describtion">String to describe this action mainly for debugging</param>
    /// <param name="time">Time of action</param>
    /// <param name="go">game object connected to action</param> //TODO unsure if needed
    /// <param name="e">Enum used to describe action</param>
    public Action(String desc, float time, GameObject go, possibleActions e)
    {
        this.gameObjectOfAction = go;
        this.timeOfAction = time;
        this.actionDescribtion = desc;
        this.enumForAction = e; 
    }

    public override string ToString()
    {
        String returnString = actionDescribtion + ", " + timeOfAction + " ," + enumForAction + " ," + gameObjectOfAction;
        return returnString;
    }

    public possibleActions GetPossibleAction()
    {
        return enumForAction;
    }

    public float GetTime()
    {
        return timeOfAction;
    }

    public string getDescription()
    {
        return actionDescribtion;
    }




}
