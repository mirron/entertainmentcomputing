﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionEvaluation
{
    private List<Action> allActions = new List<Action>();
    private int earnedPoints = 0;
    private bool bestCourseOfActions = true;
    private int timePoints = 50;
    public string EvaluteAllActions()
    {
        string outputString = "";
        allActions = Logging.getListOfAllDoneActions();

        //------------------------------------------Evaluate 911------------------------------------------\\

        Action nineOneIneCalled = allActions.Find(a => a.GetPossibleAction().Equals(possibleActions.Called911));

        if (nineOneIneCalled != null)
        {
            outputString += "Calling 911 was a good call. This can save your live during catastrophic situations and should always " +
                "be done if possible \n.";
            outputString += "You earned 10 points for this. \n \n";
            earnedPoints += 10;
        }
        else
        {
            outputString += "You should always call 911 if calamity is happening, if possible. \n \n";
            bestCourseOfActions = false;
        }

        //------------------------------------------Evaluate Earthquake------------------------------------------\\

        Action hideUnderTable = allActions.Find(a => a.GetPossibleAction().Equals(possibleActions.HideUnderTable));
        Action hideUnderDoor = allActions.Find(a => a.GetPossibleAction().Equals(possibleActions.HideUnderDoorway));
        Action leftSafeZone = allActions.Find(a => a.GetPossibleAction().Equals(possibleActions.LeftSafeZoneDuringEarthquake));

        if (hideUnderTable != null || hideUnderDoor != null)
        {
            outputString += "The safest thing to do during an earthquake would be to run to an open area. " +
                "However, this is not always possible, as in the situation at hand \n.";
            if (hideUnderTable != null)
            {
                outputString += "Hiding under a table during an earthquake is a valid choice, but you should choose a table " +
                    "that is stable enough to withstand falling rubble.\n";
                outputString += "You earned 10 points for this. \n \n";
                earnedPoints += 10;
            }
            if (hideUnderDoor != null)
            {
                outputString += "Hiding under a doorway during an earthquake is a valid choice. Doorways can protect you from" +
                    "falling rubble.\n";
                outputString += "You earned 10 points for this. \n \n";
                earnedPoints += 10;
            }
        }
        else if (leftSafeZone != null)
        {
            outputString += "The safest thing to do during an earthquake would be to run to an open area. " +
                "However, this is not always possible, as in the situation at hand. \n.";
            outputString += "Possible covers could be for example: stable tables or doorways.";
            outputString += "Nonetheless you should not leave a safe zone during an earthquake! \n \n.";
            bestCourseOfActions = false;
        } else
        {
            outputString += "The safest thing to do during an earthquake would be to run to an open area. " +
                "However, this is not always possible, as in the situation at hand .\n.";
            outputString += "Possible covers could be for example: stable tables or doorways.";
            outputString += "Nonetheless you should not have left the cover during the earthquake! \n \n.";
            bestCourseOfActions = false;
        }

        //------------------------------------------Evaluate Escape------------------------------------------\\
        Action jummpedFromBalcony = allActions.Find(a => a.GetPossibleAction().Equals(possibleActions.WinByJumping));
        Action doorfireWasExtinguished = allActions.Find(a => a.GetPossibleAction().Equals(possibleActions.DoorFireExtinguished));
        Action escaped = allActions.Find(a => a.GetPossibleAction().Equals(possibleActions.Win));

        timePoints -= (int)MainGameLogic.GetTimer();
        if (timePoints < 0)
        {
            timePoints = 0;
        }

        if (doorfireWasExtinguished != null && jummpedFromBalcony != null)
        {
            outputString += "Congratulations, you managed to live for at least another day! \n \n";
            outputString += "Jumping from a balcony or out of a window can be a valid escape tactic. A healthy human usually " +
                "survives a jump from the 3rd floor, but he/she may break his/her legs. This however is prefable to being burned " +
                "alive or diying to a carbon monoxide (CO) poisoning.\n";
            outputString += "Nonetheless in this situation jumping from the balcony was not a goos decision, since the fire in front" +
                "of the door was already extinguished and you could simply have used the starways. \n";
            outputString += "You earned 5 points for this. \n \n";
            earnedPoints += 5;
            bestCourseOfActions = false;

            outputString += "You earned " + timePoints + " bonus points for your completion time. \n \n";
            earnedPoints += timePoints;

        } else if (doorfireWasExtinguished == null && jummpedFromBalcony != null)
        {
            outputString += "Congratulations, you managed to live for at least another day! \n \n";
            outputString += "Jumping from a balcony or out of a window can be a valid escape tactic. A healthy human usually " +
               "survives a jump from the 3rd floor, but he/she may break his/her legs. This however is preferable to being burned " +
               "alive or dying to a carbon monoxide (CO) poisoning.\n";
            outputString += "Nonetheless trying to extinguish the fire in front of the entrance and escaping via stairway would" +
                "have been a smarter decision. \n";
            outputString += "You earned 10 points for this. \n \n";
            earnedPoints += 10;
            bestCourseOfActions = false;

            outputString += "You earned " + timePoints + " bonus points for your completion time. \n \n";
            earnedPoints += timePoints;

        } if (doorfireWasExtinguished != null && escaped != null)
        {
            outputString += "Congratulations, you managed to live for at least another day! \n \n";
            outputString += "Good job! You managed to take the best escape route out of the apartment for your current situation. \n";
            outputString += "You earned 20 points for this. \n \n";
            earnedPoints += 20;

            outputString += "You earned " + timePoints + " bonus points for your completion time. \n \n";
            earnedPoints += timePoints;

        }

        //------------------------------------------Evaluate Death------------------------------------------\\

        Action diedToCarbonMonoxidePoisoning = allActions.Find(a => a.GetPossibleAction().Equals(possibleActions.SmokedOut));
        Action burnedToDeath = allActions.Find(a => a.GetPossibleAction().Equals(possibleActions.WalkedIntoFire));

        if (diedToCarbonMonoxidePoisoning != null)
        {
            outputString += "You took to long to escape, because of that you died from a carbon monoxide (CO) poisoning. \n \n";
            bestCourseOfActions = false;
        } else if (burnedToDeath != null)
        {
            outputString += "YOU SHALL NOT WALK TROUGH THE FIRE AND FLAMES !!! \n \n";
            outputString += "You burned to dead, do not go to close to the fire. \n \n";
            bestCourseOfActions = false;
        }

        //------------------------------------------Best course of Action------------------------------------------\\

        if (bestCourseOfActions)
        {
            outputString += "You took the best course of actions possible for this scenario.\n";
            outputString += "Good job!\n \n";
            outputString += "You earned 50 points for this. \n \n";
            earnedPoints += 50;
        }

        return outputString;
    }

    public String getPoints()
    {
        return earnedPoints.ToString();
    }
}
