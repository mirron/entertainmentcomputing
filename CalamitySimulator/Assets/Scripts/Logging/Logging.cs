﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author Tim Kratky
public class Logging : MonoBehaviour
{
    private static List<Action> loggedAction = new List<Action>();
    public static bool win = false;

    /// <summary>
    /// Method to add a action into the logged list.
    /// </summary>
    /// <param name="a">Action which is logged</param>
    public static void setDoneAction(Action a)
    {
        loggedAction.Add(a);
    }


    /// <summary>
    /// Get List for endscreen and only then
    /// </summary>
    /// <returns>List with all Actions</returns>
    public static List<Action> getListOfAllDoneActions()
    {
        return loggedAction;
    }

    public static void ResetLogger()
    {
        loggedAction = new List<Action>();
        win = false;
    }
}