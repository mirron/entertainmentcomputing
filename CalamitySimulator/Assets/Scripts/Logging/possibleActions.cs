﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Author Tim Kratky
/// <summary>
/// Enum Class with one enum for each possible action doable in game which is relevant for the endscreen and summary / rating of the playeractions
/// </summary>
public enum possibleActions
{
    GameStart,
    GameEnd,
    FireextinguisherUsed,
    FireExtinguished,
    DoorFireExtinguished,
    WindowOpend,
    Called911,
    HideUnderTable,
    HideUnderDoorway,
    DoorOpend,
    DoorClosed,
    Win,
    Lose,
    LeftSafeZoneDuringEarthquake,
    WalkedIntoFire,
    SmokedOut,
    WinByJumping,
    CheatedLoose,
    CheatedWin,
    Cheated911Call,
    CheatToRemovedEarthQuake,
    CheatToActivateEarthQuake,
    CheatDMGImmune,
    CheatResetSmoke,
    CheatDeactivateSmoke,
    CheatRemovedFire,
    CheatingEnabled,
    CheatFireIgnited,
    CheatSpreadedFireExtinguished,
    CheatSpreadedFireReignited
    //TODO to be continued
}
