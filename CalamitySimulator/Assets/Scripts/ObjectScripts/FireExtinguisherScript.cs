﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Author Oliver Sander

/// <summary>
/// Extends the standard PlayerInteractionInverse script to allow to use the object with a leftclick
/// </summary>
public class FireExtinguisherScript : PlayerInteractionInverse
{
    private bool isSteamSpawned = false;
    private string resourcePath = "Prefabs/Steam";
    private GameObject steamGameObject = null;
    public float maxUseTime = 5.0f;
    private float currentUseTime = 0.0f;
    public AudioClip fireExClip;
    public AudioSource fireExSource;



    /// <summary>
    /// Gets the second child with is the steam and deactivates it and its children 
    ///
    /// </summary>
    void Start()
    {
        transform.GetChild(0).transform.Find("Steam").gameObject.active = false;
        //transform.GetChild(1).gameObject.active = false;
        fireExSource.clip = fireExClip;
        fireExSource.Play();
        fireExSource.Pause();
    }

    /// <summary>
    /// If leftclick is hold the steam is activated,
    /// if realased it is deactivated
    /// </summary>
    protected override void useObject()
    {
        if (Input.GetMouseButtonDown(0) && this.alreadyGrapping && !isSteamSpawned && currentUseTime < maxUseTime)
        {

            //Debug.Log("Left is clicked");
            isSteamSpawned = true;
            transform.GetChild(0).transform.Find("Steam").gameObject.active = true;
            transform.GetChild(0).transform.Find("CustomPivot").gameObject.transform.Rotate(new Vector3(-26.0f, -85.0f, -0.0f), Space.Self);
            currentUseTime += Time.deltaTime;

            if (!fireExSource.isPlaying)
            {

                fireExSource.UnPause();
            }

        }
        else if ((Input.GetMouseButtonUp(0) && isSteamSpawned) || (currentUseTime > maxUseTime && isSteamSpawned) || (!PlayerInteractWithObjects.isGrapping && isSteamSpawned))
        {
            //Debug.Log("Left is released");
            isSteamSpawned = false;
            transform.GetChild(0).transform.Find("Steam").gameObject.active = false;
            //transform.GetChild(0).transform.FindChild("CustomPivot").gameObject.transform.Rotate(new Vector3(+26.0f, +85.0f, +0.0f), Space.Self);
            transform.GetChild(0).transform.Find("CustomPivot").gameObject.transform.localRotation = new Quaternion(0f, 0f, 0f, 0f);

            fireExSource.Pause();
        }
        else if (Input.GetMouseButton(0) && this.alreadyGrapping && isSteamSpawned && currentUseTime < maxUseTime)
        {
            currentUseTime += Time.deltaTime;
            //Debug.Log("current use time: " + currentUseTime);
        }

    }
}
