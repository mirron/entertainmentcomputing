﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideUnderTableScript : MonoBehaviour
{
    bool isHidding = false;
    Vector3 positionBeforeHidding;

    private bool enteredeDuringEarthquake = false;
    private bool leftDuringEarthquake = false;
    private bool earthQuakeWasLogged = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /// <summary>
    /// As long as the player is inside the collision sphere the following functionality can be executed
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "testPlayerImmune")
        {
            //Debug.Log("Is Inside");
            if (Input.GetMouseButton(1))
            {
                if (isHidding)
                {
                    // Debug.Log("E is pressed open");
                    isHidding = false;
                    other.gameObject.transform.SetPositionAndRotation(positionBeforeHidding, new Quaternion(0, 0, 0, 0));
                    GlobalFlagScript.isMovementFrozen = false;
                    
                }
                else
                {
                    // Debug.Log("E is pressed closed");
                    isHidding = true;
                    positionBeforeHidding = other.gameObject.transform.position;
                    other.gameObject.transform.SetPositionAndRotation(this.gameObject.transform.position, other.gameObject.transform.rotation);
                    other.gameObject.transform.Rotate(new Vector3(-90, 0, 90));
                    other.gameObject.transform.Translate(0, (-0.5f), 0);
                    GlobalFlagScript.isMovementFrozen = true;
                    
                }
            }
        }

        if (isHidding && GlobalFlagScript.isEarthquake)
        {
            enteredeDuringEarthquake = true;
        }
        if (!isHidding && enteredeDuringEarthquake && GlobalFlagScript.isEarthquake && !earthQuakeWasLogged)
        {
            leftDuringEarthquake = true;
            Logging.setDoneAction(new Action("Table You should not leave the safe area during an earthquake", 0, null, possibleActions.LeftSafeZoneDuringEarthquake));
            earthQuakeWasLogged = true;
        }
        if (!isHidding && enteredeDuringEarthquake && !leftDuringEarthquake && !GlobalFlagScript.isEarthquake && !earthQuakeWasLogged)
        {
            Logging.setDoneAction(new Action("Table Stayed in safe zone during earthquake", 0, null, possibleActions.HideUnderTable));
            earthQuakeWasLogged = true;
        }
    }
}
