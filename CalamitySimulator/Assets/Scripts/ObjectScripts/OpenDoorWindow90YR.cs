﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoorWindow90YR : MonoBehaviour
{
    bool isOpen = false;
    bool doAnimation = false;

    private void Update()
    {
    }

    private void Start()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
    }

    /// <summary>
    /// As long as the player is inside the collision sphere the following functionality can be executed
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "testPlayerImmune")
        {            
            //Debug.Log("Is Inside");
            if (Input.GetMouseButton(1))
            {
                if (isOpen)
                {
                    // Debug.Log("E is pressed open");
                    isOpen = false;
                    this.gameObject.transform.Rotate(+0f, +90, +0f, Space.World);
                    this.gameObject.transform.Translate(+0.5f, +0f, -0.5f, Space.World);

                }
                else
                {
                    // Debug.Log("E is pressed closed");
                    isOpen = true;
                    this.gameObject.transform.Translate(-0.5f, -0f, +0.5f, Space.World);
                    this.gameObject.gameObject.transform.Rotate(-0f, -90f, -0f, Space.World);
                }
            }
        }
    }
}
