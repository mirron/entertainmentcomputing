﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Author Oliver Sander & Tim Kratky
/// <summary>
/// Script to enable the player to interact with gameobjects
/// </summary>
public class PlayerInteractionInverse : MonoBehaviour
{
    protected GameObject playerObject = null;
    protected bool alreadyGrapping = false;
    [SerializeField] float grapDistance = 150;
    protected GameObject playerInGrabZone;

    private void Start()
    {
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && alreadyGrapping == false && !PlayerInteractWithObjects.isGrapping)
        {
            if (grabObject())
            {
                alreadyGrapping = true;
                PlayerInteractWithObjects.isGrapping = true;
            }
        }
        else if (Input.GetKeyDown(KeyCode.E) && alreadyGrapping == true && PlayerInteractWithObjects.isGrapping)
        {
            if (releaseObject())
            {
                alreadyGrapping = false;
                PlayerInteractWithObjects.isGrapping = false;
            }
        }

        useObject();
    }

    /// <summary>
    ///  used to grap gameobjects
    /// </summary>
    public bool grabObject()
    {

        playerObject = playerInGrabZone;
        if (playerObject != null)
        {
            this.transform.parent = playerObject.transform;
            this.transform.position = playerObject.transform.GetChild(2).position;
            this.transform.rotation = playerObject.transform.GetChild(2).rotation;

            //grappedObject.transform.parent = this.transform;
            print("Grapped " + playerObject.name);
            return true;
        }
        return false;

    }

    /// <summary>
    /// used to release the grapped object again
    /// </summary>
    public bool releaseObject()
    {
        if (playerObject != null)
        {
            this.transform.parent = null;
            print("released " + playerObject.name);
            playerObject = null;
            return true;
        }
        return false;
    }


    /// <summary>
    /// returns nearest gameobject with given tag to the worldposition of this gameobject
    /// </summary>
    /// <param name="tag"></param>
    /// <returns></returns>
    public GameObject findNextInteractableGamoObject(string tag)
    {

        GameObject[] potentialObjects = GameObject.FindGameObjectsWithTag(tag);
        GameObject closest = null;
        float dist = Mathf.Infinity;
        //float dist = grapDistance;
        Vector3 position = transform.position;

        foreach (GameObject go in potentialObjects)
        {

            //TODO grapping behaves strange -  both ways result in same strange behavoir

            //Vector3 diff = go.transform.position - position;
            //print(diff + "  " + go.name);
            //float curDistance = diff.sqrMagnitude;
            //print(curDistance + "  " + go.name);
            //if (curDistance < dist)
            //{
            //    closest = go;
            //    dist = curDistance;
            //}
            if (closest == null && Vector3.Distance(transform.position, go.transform.position) <= grapDistance)
            {
                closest = go;
            }
            else if (Vector3.Distance(transform.position, go.transform.position) <= Vector3.Distance(transform.position, closest.transform.position) && Vector3.Distance(transform.position, closest.transform.position) <= grapDistance)
            {
                closest = go;
            }
        }
        return closest;
    }

    /// <summary>
    /// checks if a player enters the collison sphere
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        // Debug.Log("object in Zone");

        if (other.gameObject.tag == "Player" || other.gameObject.tag == "testPlayerImmune")
        {
            // Debug.Log("Tag Correct in");
            playerInGrabZone = other.gameObject.gameObject;
        }
    }

    /// <summary>
    /// checks if a player left the collison sphere
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerExit(Collider other)
    {
        // Debug.Log("object out of Zone");
        if (other.gameObject.gameObject.tag == "Player" || other.gameObject.tag == "testPlayerImmune")
        {
            // Debug.Log("Tag Correct out");
            playerInGrabZone = null;
        }
    }

    /// <summary>
    /// Override this method in a child with the stuff that should happen if you use it
    /// Look at the fire extinguisher script for an example
    /// </summary>
    protected virtual void useObject()
    {

    }
}
