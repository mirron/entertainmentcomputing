﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmartPhoneScript : PlayerInteractionInverse
{
    private bool is911Called = false;
    public AudioClip nineOneOneClip;
    public AudioSource nineOneOneSource;
    public AudioClip fireFighterClip;
    public AudioSource fireFighterSource;
    public float fireFighterTime = 10.0f;
    public float timer = 0.0f;


    /// <summary>
    /// Gets the second child with is the steam and deactivates it and its children 
    ///
    /// </summary>
    void Start()
    {
        nineOneOneSource.clip = nineOneOneClip;
        fireFighterSource.clip = fireFighterClip;
    }

    /// <summary>
    /// If leftclick is hold the steam is activated,
    /// if realased it is deactivated
    /// </summary>
    protected override void useObject()
    {
        if (Input.GetMouseButtonUp(0) && !is911Called && this.alreadyGrapping)
        {
            is911Called = true;
            nineOneOneSource.Play();
            Logging.setDoneAction(new Action("Fire department was called", 0, null, possibleActions.Called911));
        }

        if (is911Called)
        {
            if (timer < fireFighterTime)
            {
                timer += Time.deltaTime;
            } else if (!fireFighterSource.isPlaying)
            {
                fireFighterSource.Play();
            }
            
        }
    }
}
