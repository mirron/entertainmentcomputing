﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Author Tim Kratky
/// Class to simulate different states of the game / make it easier to inspect / debug and test unusual states of the game
/// </summary>
public class CheatScript : MonoBehaviour
{


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.F1) && GlobalFlagScript.enableCheating == true)
        {
            //manipulate fire
            
            print("manipulate fire");
            GameObject go = GameObject.FindGameObjectWithTag("KitchenFire");
            GameObject go2 = GameObject.FindGameObjectWithTag("DoorFire");
            GameObject go3 = GameObject.FindGameObjectWithTag("NormalFire");
            if (go != null || go2 != null || go3 != null )
            {
                if (go != null)
                {
                    Logging.setDoneAction(new Action("KitchenFire extinguished", MainGameLogic.GetTimer(), null, possibleActions.FireExtinguished));
                    Destroy(go);
                }
                if (go2 != null)
                {
                    Logging.setDoneAction(new Action("DoorFire extinguished", MainGameLogic.GetTimer(), null, possibleActions.DoorFireExtinguished));
                    Destroy(go2);
                }
                if (go3 != null)
                {
                    Logging.setDoneAction(new Action("Normal / Spreaded Fire extinguished", MainGameLogic.GetTimer(), null, possibleActions.CheatSpreadedFireExtinguished));
                    Destroy(go3);
                }

                MainGameLogic.fireManager.ResetSpawnedFire();
            }
            else
            {   
                //respawning fire if f1 is pressed again
                Logging.setDoneAction(new Action("DoorFire reignited", MainGameLogic.GetTimer(), null, possibleActions.CheatFireIgnited));
                Logging.setDoneAction(new Action("SpreadedFire reignited", MainGameLogic.GetTimer(), null, possibleActions.CheatSpreadedFireReignited));
                
                MainGameLogic.fireManager.SpawnFire();
            }
        }
        if (Input.GetKeyUp(KeyCode.F2) && GlobalFlagScript.enableCheating == true)
        {
            //deactivate smoke
            print("deactivate smoke");
            Logging.setDoneAction(new Action("Cheat used to remove smoke", MainGameLogic.GetTimer(), null, possibleActions.CheatDeactivateSmoke));
            GameObject go = GameObject.FindGameObjectWithTag("SmokeEffect");
            if (go != null)
            {
                go.SetActive(false);
            }

        }
        if (Input.GetKeyUp(KeyCode.F3) && GlobalFlagScript.enableCheating == true)
        {
            //reset smoke
            print("reset smoke");
            Logging.setDoneAction(new Action("Cheat used to reset smoke", MainGameLogic.GetTimer(), null, possibleActions.CheatResetSmoke));
            GameObject go = GameObject.FindGameObjectWithTag("SmokeEffect");
            if (go != null)
            {
                ParticleSystem ps = go.GetComponent<ParticleSystem>();
                ps.Clear();
                ps.Play();
            }


        }
        if (Input.GetKeyUp(KeyCode.F4) && GlobalFlagScript.enableCheating == true)
        {
            //immune to dmg
            print("immunbe to dmg");
            Logging.setDoneAction(new Action("Cheat used be immune to dmg", MainGameLogic.GetTimer(), null, possibleActions.CheatDMGImmune));
            GameObject go = GameObject.FindGameObjectWithTag("Player");
            if (go != null)
            {
                go.tag = "testPlayerImmune";
            }

        }
        if (Input.GetKeyUp(KeyCode.F5) && GlobalFlagScript.enableCheating == true)
        {
            //manipulate earth quake 
            print("manipulate earthquake");
            
            if (!GlobalFlagScript.isEarthquake)
            {
                Logging.setDoneAction(new Action("Cheat used to activate earthquake", MainGameLogic.GetTimer(), null, possibleActions.CheatToActivateEarthQuake));
                MainGameLogic.StartEarthquake();
                MainGameLogic.fireManager.SpawnFire();
            }
            else if (GlobalFlagScript.isEarthquake)
            {
                Logging.setDoneAction(new Action("Cheat used to remove earthquake", MainGameLogic.GetTimer(), null, possibleActions.CheatToRemovedEarthQuake));
                MainGameLogic.StopEarthquake();
            }


        }
        if (Input.GetKeyUp(KeyCode.F6) && GlobalFlagScript.enableCheating == true)
        {
            //called 911
            print("called 911");
            Logging.setDoneAction(new Action("Cheat used to call 911", MainGameLogic.GetTimer(), null, possibleActions.Cheated911Call));
            Logging.setDoneAction(new Action("called 911", MainGameLogic.GetTimer(), null, possibleActions.Called911));

        }
        if (Input.GetKeyUp(KeyCode.F7) && GlobalFlagScript.enableCheating == true)
        {
            //auto win via safezone        
            Logging.setDoneAction(new Action("Cheat used to win via safezone", MainGameLogic.GetTimer(), null, possibleActions.CheatedWin));
            Logging.win = true;
            Logging.setDoneAction(new Action("Reached safezone", 0, null, possibleActions.Win));
            SceneManager.LoadScene(GlobalFlagScript.EndScene);
        }

        if (Input.GetKeyUp(KeyCode.F8) && GlobalFlagScript.enableCheating == true)
        {
            //auto win via jumping       
            Logging.setDoneAction(new Action("Cheat used to win via jumping", MainGameLogic.GetTimer(), null, possibleActions.CheatedWin));
            Logging.win = true;
            Logging.setDoneAction(new Action("Jumping Down can be a valid choise, but you a risking to break your legs, or worse", 0, null, possibleActions.WinByJumping));
            SceneManager.LoadScene(GlobalFlagScript.EndScene);
        }

        if (Input.GetKeyUp(KeyCode.F9) && GlobalFlagScript.enableCheating == true)
        {
            //auto lose via fire contact
            Logging.setDoneAction(new Action("Cheat used to loose via walking inside the fire", MainGameLogic.GetTimer(), null, possibleActions.CheatedLoose));
            SceneManager.LoadScene(GlobalFlagScript.EndScene);
            Logging.setDoneAction(new Action("You shall not walk trough the fire and the flame", 0, this.gameObject, possibleActions.WalkedIntoFire));

        }
        if (Input.GetKeyUp(KeyCode.F10) && GlobalFlagScript.enableCheating == true)
        {
            //auto lose via timeout
            Logging.setDoneAction(new Action("Cheat used to loose", MainGameLogic.GetTimer(), null, possibleActions.CheatedLoose));
            Logging.setDoneAction(new Action("You took to long and could not breath becuase of the smoke", 0, this.gameObject, possibleActions.SmokedOut));
            SceneManager.LoadScene(GlobalFlagScript.EndScene);
        }

        if (Input.GetKeyUp(KeyCode.F11))
        {
            // enable cheating
            print("enable cheating");
            Logging.setDoneAction(new Action("Cheating now allowed", MainGameLogic.GetTimer(), null, possibleActions.CheatingEnabled));
            GlobalFlagScript.enableCheating = true;

        }
    }
}
