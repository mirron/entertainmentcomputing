﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//done with https://www.youtube.com/watch?v=n-KX8AeGK7E
/// <summary>
/// Script to implement the playercamera und rotate it by mousemovent
/// </summary>
public class PlayerCamera : MonoBehaviour
{
    //serialize to see field in the inspector
    [SerializeField] private string mouseXName = "Mouse X";
    [SerializeField] private string mouseYName = "Mouse Y";
    [SerializeField] private float sensitivity = 150;
    [SerializeField] private Transform playerBody;
    private Vector3 cameraOriginalPosition;
    private Vector3 totalCameraMovment = new Vector3(0, 0, 0);
    private float maxCameraShaking = 0.25f;

    private float xAxisClamp;

    /// <summary>
    /// lock cursor at start of the game in the center of view
    /// </summary>
    private void lockCursor()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Awake()
    {
        lockCursor();
        xAxisClamp = 0;
    }


    // Update is called once per frame
    void Update()
    {
        if (!GlobalFlagScript.isMovementFrozen)
        {
            rotation();

            cameraOriginalPosition = transform.position;
            //Camera shaking during earthquake
            if (GlobalFlagScript.isEarthquake)
            {
                float randomX = Random.Range(-2f, 2f) * 0.1f;
                float randomY = Random.Range(-2f, 2f) * 0.1f;
                float randomZ = Random.Range(-2f, 2f) * 0.1f;
                Vector3 shakedPostionFactor = new Vector3(randomX, randomY, randomZ);
                //Debug.Log(shakedPostionFactor.ToString());
                transform.position = transform.position + shakedPostionFactor;
                totalCameraMovment += shakedPostionFactor;

                if (totalCameraMovment.x > maxCameraShaking || totalCameraMovment.y > maxCameraShaking || totalCameraMovment.z > maxCameraShaking ||
                    totalCameraMovment.x < -maxCameraShaking || totalCameraMovment.y < -maxCameraShaking || totalCameraMovment.z < -maxCameraShaking)
                {
                    transform.position -= totalCameraMovment;
                    totalCameraMovment = new Vector3(0, 0, 0);
                }
                //transform.position = cameraOriginalPosition;
            }
            else if (!GlobalFlagScript.isEarthquake && (totalCameraMovment.x != 0 || totalCameraMovment.y != 0 || totalCameraMovment.z != 0))
            {
                transform.position -= totalCameraMovment;
                totalCameraMovment = new Vector3(0, 0, 0);
            }
        }
        
    }

    /// <summary>
    /// method to manage the camera rotation and stop it from moving more then a normal human could do (no 360° turns on y axis )
    /// </summary>
    private void rotation()
    {
        float mouseX = Input.GetAxis(mouseXName) * sensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis(mouseYName) * sensitivity * Time.deltaTime;

        // remove flipping camera by to much moving
        xAxisClamp += mouseY;
        if (xAxisClamp > 90.0f)
        {
            xAxisClamp = 90.0f;
            mouseY = 0.0f;
            stopCameraFromMovingToMuch(270.0f);
        }

        else if (xAxisClamp < -90.0f)
        {
            xAxisClamp = -90.0f;
            mouseY = 0.0f;
            stopCameraFromMovingToMuch(90.0f);
        }

        //movement of camera
        transform.Rotate(Vector3.left * mouseY);
        playerBody.Rotate(Vector3.up * mouseX);
    }

    /// <summary>
    /// used to stop the camera from allowing to much rotation
    /// </summary>
    /// <param name="value"></param>
    private void stopCameraFromMovingToMuch(float value)
    {
        Vector3 eulerRotation = transform.eulerAngles;
        eulerRotation.x = value;
        transform.eulerAngles = eulerRotation;
    }
}
