﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//done with https://www.youtube.com/watch?v=n-KX8AeGK7E jumping is also described inside this video starting at 16:30
/// <summary>
/// Script to manage the player movement (wasd) 
/// </summary>
public class PlayerMovement : MonoBehaviour
{
    private string horizontalName = "Horizontal";
    private string verticalName = "Vertical";
    private CharacterController cont;
    private static GameObject playerObject;

    [SerializeField] private float speed = 5;

    private void Awake()
    {
        cont = GetComponent<CharacterController>();
    }

    // Start is called before the first frame update
    void Start()
    {
        playerObject = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        move();
    }

    /// <summary>
    /// playermovement by given input and set speed value
    /// </summary>
    private void move()
    {
        if (!GlobalFlagScript.isMovementFrozen)
        {
            float vertInput = Input.GetAxis(verticalName) * speed;
            float horInput = Input.GetAxis(horizontalName) * speed;

            Vector3 forward = transform.forward * vertInput;
            Vector3 right = transform.right * horInput;

            cont.SimpleMove(forward + right);
        }

    }
}
