﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SafeZoneScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "testPlayerImmune")
        {
            // TODO: Make the player Win 
            Logging.win = true;
            Logging.setDoneAction(new Action("Reached safezone", 0, null, possibleActions.Win));
            SceneManager.LoadScene(GlobalFlagScript.EndScene);
        }
    }
}
