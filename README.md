# EntertainmentComputing

## Ingame Movement and Actions

* Movement via WASD without Jumping
* Grabbing Objects with E 
* Interacting with grabbed Objects with left mouse button
* Interact with non grabbed Object in gameworld right mouse button


## Player

Right now the player is an empty gameobject with contains a camera and a body. The body is just a dummy to have and kind of indicator that there is a player / you might can see yourself. 

Attached to the playergameobject is the player Player Movement Script which handles just the movement. Here you can tweak the movementspeed of the player which is set to a default at 5 (inside the script). 

Attached to the PlayerCamera is the PlayerCamera Script which handles the viewpoint and playercamera. Here you can manipulate the Mousesensivity. 

If you want to create a new player you have to set the PlayerBody attribute in the player camera script (just drag and drop the emptygameobject from the sample scene to the inspector field representing the playerbody attribute. 

The cheatscript need to be added to the player.

## Usable Objects 

The doors / double doors, the smartphone, the fireextinguisher are use - grabable. If this game would be more than a prototype more objects would be added to the game as example an additional idea another object might be a towel which could interact with water to stop the smoke spreading a little bit. 

## Cheats

The cheats might break the game so use them careful / do not be surprised by unwanted behavior. 

| Hotkey | What it is used for |
|--------|---------------------|
|F1| manipulate fire, removing / respawning it |
|F2| deactivate smoke  |
|F3| reset smoke  |
|F4| immune to fire damage|
|F5| manipulate earthquake, stopping the earthquake / starting the earthquake and the connected door fire|
|F6| set 911 call as done|
|F7| automatic win via safezone|
|F8| automatic win via jumping|
|F9| automatic lose via firecontact|
|F10| automatic lose via timeout|
|F11| enable cheating|

## Game explanation

The first thing you hear is a siren. This signals, that an earthquake will start soon, so you should look for cover under the doorways or the table. 
Afterwards you will hear the fire alarm and the fire starts spreading.
Now you have multiple actions you can take. First you can go into the workroom and grab the phone to call 911. Furthermore you can grab the fire extinguisher and use it to extinguish  fire, except the kitchen fire. But be careful you can not use the extinguisher indefinitely.
If you walk into the fire you die, surprse! If you take to long too escape you will die of carbon monoxide poisoning.
You have 120 seconds to escape. To escape you can either use the exit door and take the stairway to safety. Alternatively you can jump down the balcony (We are aware that balconies usale have railings, so just do not take you kids into this apartment).

## Project Structure

- Assets
  - fonts (Contains all used fonts)
  - image (Contains all used images for the start and end screen)
  - ParticleSystem (Unitys ParticlePack from the asset store, contains various particle systems)
  - Resources
	- Audio (Contains all audio files used for the game)
	- Materials (Contains all materials created for the game)
	- Models (Contains the apartment and fire extinguisher model)
	- Prefabs (Contains all prefabs that were created for the game)
  - Scenes (Contains the three main scenes, start, level and end)
	- TestScenes (Contains various test scenes created for the game, which were used to test certain functionalities. Some of those may not work correctly anymore.)
  - Scripts (Contains all Scripts, scripts that are directly in this folder were not categorized)
    - FireScripts (Contains the FireManager and all scripts related to fire)
    - GameLogicScripts (Contains the main logic scripts for all scenes)
    - Logging (Contains all scripts that handle the logging)
    - ObjectScripts (Contains all the scripts for interactable and usable objects, e.g. opendoors, grab objects, use grabbed objects like the smartphone)
    - PlayerScripts (Contains all the scripts responsible for player and camera movement, as well as the cheat script)
  - Tree_Textures (Contains the tree textures)
  - UI (Also contains images used for the start and end screen)



